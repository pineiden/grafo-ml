from grafo_ml.vertice import AbstractVertice
from grafo_ml.arista import AbstractArista
from attrs import define, fields
from pathlib import Path
import csv
from rich import print
from pydantic.dataclasses import dataclass
from grafo_ml.grafo import Grafo, ConfigGrafo


@define(frozen=True)
class Animal:
    id: int
    tipo: str
    ambiente: str
    edad: int
    hijos: int


class Vertice(AbstractVertice):

    @property
    def fields(self):
        return {f.name for f in fields(type(self.element))}

    @property
    def element_class(self):
        return Animal

    @property
    def numeric_fields(self):
        Numeric = {int, float}
        return {f.name for f in fields(type(self.element)) if f.type in Numeric}

    def __len__(self):
        return 1


class Arista(AbstractArista):
    def similitud(self):
        return 1


if __name__ == "__main__":
    path = Path(__file__).parent / "animalia.csv"
    with path.open() as f:
        reader = csv.DictReader(f)
        vertices = set()
        for i, row in enumerate(reader):
            row["id"] = i
            row["edad"] = int(row["edad"])
            row["hijos"] = int(row["hijos"])
            animal = Animal(**row)
            vertice = Vertice(animal)
            vertices.add(vertice)

    pesos = {
        "ambiente": .2,
        "tipo": .3,
        "edad": .3,
        "hijos": .2
    }
    limite = 0.4
    config = ConfigGrafo(
        ["ambiente", "tipo"],
        Vertice,
        Arista,
        pesos,
        limite)
    g = Grafo.create("Animalia", vertices, config)
    print(g)
    for vertice, aristas in g.mapa.items():
        print(vertice, "vecinos->", g.vecinos(vertice))
