from grafo_ml.vertice import Vertice
from grafo_ml.arista import Arista, AbstractArista

import unittest


class TestArista(unittest.TestCase):
    def test_create(self):
        v1 = Vertice("David")
        v2 = Vertice("Josefa")
        arista = Arista.create(v1, v2)
        self.assertTrue(isinstance(arista, AbstractArista))

    def test_similitud(self):
        v1 = Vertice("David")
        v2 = Vertice("Josefa")
        arista = Arista.create(v1, v2)
        self.assertTrue(arista.similitud == 1)
