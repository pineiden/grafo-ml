from grafo_ml.vertice import Vertice
import unittest


class TestVertice(unittest.TestCase):
    def test_create(self):
        v = Vertice(10)
        self.assertEqual(v.element, 10)

    def test_compare(self):
        v1 = Vertice(10)
        v2 = Vertice(100)
        v3 = Vertice("David")
        self.assertTrue(v1.compare_type(v2))
        self.assertFalse(v1.compare_type(v3))

    def test_asdict(self):
        v = Vertice(10)
        self.assertTrue(v.asdict(), {"element": 10})


if __name__ == '__main__':
    unittest.main()
