from attrs import define
from typing import Any
from .vertice import AbstractVertice
from attrs import asdict
from abc import ABC, abstractmethod

Vertice = AbstractVertice


@define(slots=True, frozen=True)
class AbstractArista(ABC):
    a: Vertice
    b: Vertice

    @classmethod
    def create(cls, a: Vertice, b: Vertice):
        assert a.compare_type(b), "Vertices no son del mismo tipo"
        return cls(a, b)

    @property
    def asdict(self):
        return asdict(self)

    @property
    @abstractmethod
    def similitud(self):
        pass

    def otra(self, vertice: Vertice):
        if vertice in {self.a, self.b}:
            if vertice == self.a:
                return self.b
            else:
                return self.a


class Arista(AbstractArista):
    """
    Para aristas cuyos vertices tengan elemento al que se pueda
    calcular 'len'
    """

    @property
    def similitud(self):
        return abs(len(self.a) - len(self.b))
