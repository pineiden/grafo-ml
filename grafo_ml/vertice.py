from attrs import define, field
from typing import Any, Dict, TypeVar, Callable
from attrs import asdict
from abc import ABC, abstractmethod
from numbers import Number

TVertice = TypeVar("TVertice", bound="Vertice")

# Esto puede cambiar en 3.11 a "Self"


@define(slots=True, frozen=True)
class AbstractVertice(ABC):
    element: Any  # must has a 'get' method
    length_callback: Callable[[Any], Number] = field(factory=lambda: len)

    @classmethod
    def create(cls, value):
        return cls(value)

    def compare_type(self, other: TVertice) -> bool:
        return type(self.element) == type(other.element)

    @property
    def asdict(self) -> Dict[str, Any]:
        return asdict(self)

    @abstractmethod
    def __len__(self):
        pass

    def get(self, field: str):
        """
        Extrae el valor del campo para que sea comparable
        """
        return getattr(self.element, field)

    @property
    @abstractmethod
    def fields(self):
        """
        Return fields considered relevent for node, with the weight
        for that field

        """
        pass

    @property
    @abstractmethod
    def element_class(self):
        pass


class Vertice(AbstractVertice):
    """
    Type of element must be str
    """

    def __len__(self):
        return self.length_callback(self.element)

    def get(self, field: str):
        return self.element

    @property
    def fields(self):
        return {}

    @property
    def element_class(self):
        return str

    @property
    def numeric_fields(self):
        return {}
