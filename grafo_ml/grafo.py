from attrs import define, field
from typing import Any, Set, Iterable, Dict, List, Set, Type
from .vertice import AbstractVertice
from .arista import AbstractArista
from itertools import groupby
from random import choice
from rich import print
import numpy as np
from scipy import spatial

Numeric = (int, float)
Vertice = AbstractVertice
Arista = AbstractArista


def similarity(u, v):
    result = 1 - spatial.distance.cosine(u, v)
    return result


@define
class ConfigGrafo:
    """
    Define los campos en orden para general clusters
    Con esto el grafo generará vértices de centralidad

    """
    clusters: List[str]
    class_vertice: Type[Vertice]
    class_arista: Type[Arista]
    pesos: Dict[str, float] = field()
    limite: float = field()

    @pesos.validator
    def check_pesos(self, attribute, value):
        assert isinstance(value, dict), "Debe ser un diccionario"
        if sum(value.values()) > 1:
            raise ValueError("""La suma de todos los valors debe ser a
                             lo más 1""")

    @limite.validator
    def check_limite(self, attribute, value):
        if not (0 <= value <= 1.0):
            raise ValueError("Solo puede ser un valor entre 0 y 1")

    @property
    def Vertice(self):
        return self.class_vertice

    @property
    def Arista(self):
        return self.class_arista


@define
class Grafo:
    name: str
    vertices: Set[Vertice]
    aristas: Set[Arista]
    mapa: Dict[Vertice, Set[Arista]]
    config: ConfigGrafo
    arista_counter: Dict[Arista, int]
    schema: Dict[str, Any]
    positions: Dict[int, Vertice]
    inverse_positions: Dict[Vertice, int]

    def add(self, verice: Vertice):
        pass

    @classmethod
    def create(cls,
               name: str,
               vertices: Iterable[Vertice],
               config: ConfigGrafo):
        vertices = set(vertices)
        clusters = [c for c in config.clusters]
        arista_counter = {}
        aristas = set()
        schema = {}
        mapa = {}
        discard = set()

        positions = {i: v for i, v in enumerate(vertices)}
        inverse_positions = {v: i for i, v in enumerate(vertices)}
        # se calcula una sola vez la tupla de vectors
        # este vector interopera
        vertices_vectors = {v: np.array(v.values) for v in vertices}

        aristas = cls.build(
            vertices,
            clusters,
            aristas,
            config.Arista,
            mapa,
            arista_counter,
            config,
            schema,
            discard,
            inverse_positions,
            vertices_vectors
        )

        return cls(
            name,
            vertices,
            aristas,
            mapa,
            config,
            arista_counter,
            schema,
            positions,
            inverse_positions)

    @property
    def asdict(self):
        return asdict(self)

    def vecinos(self, vertice: Vertice):
        return {self.positions.get(j) for i, j in self.mapa.get(vertice)}

    def add_vertice(self, vertice: Vertice):
        """
        TODO:
        Considerando los clusters
        añadir vértice en cluster correspondiente, o bien crearlo si
        no existe
        Al añadir analizar con los vecinos para crear aristas
        """
        return

    @classmethod
    def build(cls,
              vertices: Iterable[Vertice],
              clusters: List[str],
              aristas: Set[Arista],
              arista_class: Type[Arista],
              mapa: Dict[Vertice, Iterable[Arista]],
              arista_counter: Dict[Arista, int],
              config: ConfigGrafo,
              schema: Dict[str, Any] = {},
              discard: Set = set(),
              inverse_positions: Dict[Vertice, int] = {},
              error: float = 1e-6,
              vertices_vectors: Dict[Vertice, np.Array]
              ):
        """
        Dado un conjunto de vértices
        Se toma 'config' para identificar la clasificación de
        subgrafos mediante una categoria
        Primero, agrupar por cada uno de los cluster determinados en
        config
        Luego construir el subgrafo
        """
        Arista = arista_class

        def insert_arista(u, v, sim):
            #arista = Arista(u, v)
            position = (inverse_positions.get(u), inverse_positions.get(v))
            # deberia ser 1
            if position not in arista_counter:
                arista_counter[position] = 1
            else:
                arista_counter[position] += 1

            #arista = Arista(u, v)
            aristas.add(position)
            mapa[u].add(position)
            mapa[v].add(position[::-1])

        if clusters:
            """
            Por cada subagrupacion 'cluster'
            se obtiene un campo que contendrá los elementos agrupados.
            genernado un diccionario asociativo
            """
            clusters = [c for c in clusters]
            cluster = clusters.pop(0)
            discard.add(cluster)
            for key, group in groupby(vertices, lambda v:
                                      v.get(cluster)):

                group = set(group)
                if key not in schema:
                    schema[key] = {}
                if not clusters:
                    schema[key] = {g: {} for g in group}
                cls.build(
                    group,
                    clusters,
                    aristas,
                    Arista,
                    mapa,
                    arista_counter,
                    config,
                    schema[key],
                    discard,
                    inverse_positions
                )
        else:
            """
            Se escoge un vértice
            se extraen los campos considerados para la construcción
            del grafo
            se actualiza el mapa de referencias
            por cada vértice se revisan las relaciones con el resto
            generando la matriz superior (ya que la adjacencia es
            simetrica)


            """
            some_vertex = choice(list(schema.keys()))
            considered_fields = some_vertex.fields  # - discard
            mapa.update({v: set() for v in vertices})
            set_vertices = set(vertices)
            for v in vertices:
                set_vertices -= set((v,))
                for u in set_vertices:
                    valor_v = vertices_vectors.get(v)
                    valor_u = vertices_vectors.get(u)
                    sim = similarity(valor_v, valor_u)
                    if abs(sim) <= config.limite:
                        insert_arista(u, v, sim)

            # build the links at base cluster
            # generar vertice centralidad
            return aristas
